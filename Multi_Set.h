//Multi_Set.h
#pragma once
#include<set>
#include<iterator>
#include<vector>
#include<algorithm>
#include<iostream>
#include<iomanip>
#include<fstream>

using namespace std;

#define N 100

class Multi_Set 
{
private:
	multiset<int> mySet;
	int n;

public:
	Multi_Set();                 
	void Enter_Set(int n);       //random enter elements
	void Enter_Set2(int n);      //enter elements
	void Output_Set();           //show element of multyset
	~Multi_Set();				 
	void size();				 //return count of elements
	bool empty();				 //return true if multyset is empty
	void swap(Multi_Set& set1);  //swap elements between two multysets
	void include();				 //include new element
	void exlude();				 //exclude element
	void find();				 //check if element is in multyset
	void accumulate();			 //accumulate suma or multiplied all elements
	void for_each();			 //change element

	Multi_Set& operator=(Multi_Set& set1)   
	{
		set1.n = mySet.size();
		set1.mySet = mySet;
		cout << "First Multi Set = Second Multi Set : ";
		return *this;
	}

	Multi_Set& operator==(Multi_Set& set1)  
	{
		if (set1.n == mySet.size())
		{
			cout << "Multi Setes are same : 1=2 " << endl;
			
		}
		else
		{
			if (set1.n != mySet.size())
			{
				cout << "1 and 2 Multi Stes are not same : " << endl;
				if (set1.mySet.size() > mySet.size())
				{
					cout << "Multi Sets are : 2 > 1 " << endl;
				}
				else cout << "Multi Sets are : 1 > 2 " << endl;
			}
			
		}
		return *this;
	}
};