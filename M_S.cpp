//M_S.cpp
#include "Multi_Set.h"
#include <set>
#include<iterator>
#include<algorithm>
#include<vector>
#include<iostream>
#include<cstdlib>
#include<fstream>

using namespace std;

int main()
{
	srand(time_t(NULL));

	Multi_Set M1;
	int n1;
	cout << "Size of Multi Set 1 : " << endl;
	cin >> n1;
	M1.Enter_Set(n1);

	Multi_Set M2;
	int n2;
	cout << "\nSize of Multi Set 2 : " << endl;
	cin >> n2;
	M2.Enter_Set2(n2);

	cout << "\nFirst Multi Set : " << endl;
	M1.Output_Set();
	cout << endl << "\nSecond Multi Set : " << endl;
	M2.Output_Set();
	cout << endl;

	tryAgain: cout << "\nEnter what do you do : \n 0 - Add Multi Set; \n 1 - Show Mult Set;\n 2 - Show size of Multi Set;\n 3 - Show empty MUlti Set;\n 4 - Swap Multi Set;" <<
		"\n 5 - Include Multi Set;\n 6 - Exclude Multi Set;\n 7 - Find Multi Set;\n 8 - accumulate Multi Set;\n 9 - For Each Multi Set;" <<
		"\n 10 - = Multi Set;\n 11 - == or != Multi Set;\n ";

	int key;
	cin >> key;

	switch (key)
	{
	case 0:
	{
		int key1;
		cout << "What Multi Set you want to add : \n1 - first Set;\n2 - Second Set;\n";
		cin >> key1;
		if (key1 == 1)
		{
			cout << "Size of Multi Set 1 : " << endl;
			cin >> n1;
			M1.Enter_Set2(n1);
		}
		else
		{
			if (key1 == 2)
			{
				cout << "\nSize of Multi Set 2 : " << endl;
				cin >> n2;
				M2.Enter_Set2(n2);
			}
			else cout << "Error : You enter wrong key!\n";
		}
		cout << endl;
	}break;
	case 1:
	{
		int key1;
		cout << "What Multi Set you want to show : \n1 - first Set;\n2 - Second Set;\n";
		cin >> key1;
		if (key1==1)
		{
			M1.Output_Set();
			cout << endl;
		}
		else
		{
			if (key1 == 2)
			{
				M2.Output_Set();
				cout << endl;
			}
			else cout << "Error : You enter wrong key!\n";
		}
		cout << endl;
	}
		break;
	case 2:
	{
		int key1;
		cout << "What size of Multi Set you want to show : \n1 - first Set;\n2 - Second Set;\n";
		cin >> key1;
		if (key1 == 1)
		{
			M1.size();
			cout << endl;
		}
		else
		{
			if (key1 == 2)
			{
				M2.size();
				cout << endl;
			}
			else cout << "Error : You enter wrong key!\n";
		}
	}
		break;
	case 3: //empty
	{
		int key1;
		cout << "What Multi Set you want to show : \n1 - first Set;\n2 - Second Set;\n";
		cin >> key1;
		if (key1 == 1)
		{
			M1.empty();
			cout << endl;
		}
		else
		{
			if (key1 == 2)
			{
				M2.empty();
				cout << endl;
			}
			else cout << "Error : You enter wrong key!\n";
		}
	}
		break;
	case 4:  //swap
	{
		M1.swap(M2);
		cout << "\nFirst Multi Set : " << endl;
		M1.Output_Set();
		cout << endl << "\nSecond Multi Set : " << endl;
		M2.Output_Set();
	}
		break;
	case 5:  //include
	{
		int key1;
		cout << "What Multi Set you want to include : \n1 - first Set;\n2 - Second Set;\n";
		cin >> key1;
		if (key1 == 1)
		{
			M1.include();
			cout << endl;
		}
		else
		{
			if (key1 == 2)
			{
				M2.include();
				cout << endl;
			}
			else cout << "Error : You enter wrong key!\n";
		}
	}
		break;
	case 6:   //exclude
	{
		int key1;
		cout << "What Multi Set you want to exclude : \n1 - first Set;\n2 - Second Set;\n";
		cin >> key1;
		if (key1 == 1)
		{
			M1.exlude();
			cout << endl;
		}
		else
		{
			if (key1 == 2)
			{
				M2.exlude();
				cout << endl;
			}
			else cout << "Error : You enter wrong key!\n";
		}
	}
		break;
	case 7:   //find
	{
		int key1;
		cout << "What Multi Set you want to find : \n1 - first Set;\n2 - Second Set;\n";
		cin >> key1;
		if (key1 == 1)
		{
			M1.find();
			cout << endl;
		}
		else
		{
			if (key1 == 2)
			{
				M2.find();
				cout << endl;
			}
			else cout << "Error : You enter wrong key!\n";
		}
	}
		break;
	case 8:  //accumulate
	{
		int key1;
		cout << "What Multi Set you want to accumulate : \n1 - first Set;\n2 - Second Set;\n";
		cin >> key1;
		if (key1 == 1)
		{
			M1.accumulate();
			cout << endl;
		}
		else
		{
			if (key1 == 2)
			{
				M2.accumulate();
				cout << endl;
			}
			else cout << "Error : You enter wrong key!\n";
		}
	} break;
	case 9:  //for_each
	{
		int key1;
		cout << "What Multi Set you want to for each : \n1 - first Set;\n2 - Second Set;\n";
		cin >> key1;
		if (key1 == 1)
		{
			M1.for_each();
			cout << endl;
		}
		else
		{
			if (key1 == 2)
			{
				M2.for_each();
				cout << endl;
			}
			else cout << "Error : You enter wrong key!\n";
		}
	}
	break;
	case 10:  //=
	{
		M1.operator=(M2);
		M1.Output_Set();
	} break;
	case 11:   //== or !=
	{
		M1.operator==(M2);
	}break;
	default:
		break;
	}

	bool flag;
	cout << "\nEnter next action : \n 1 - other action; \n 0 - exit;\n";
	cin >> flag;

	if (flag == 1)
	{
		goto tryAgain;
	}

	return 0;
}