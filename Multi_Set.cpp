//Multi_Set.cpp
#include "Multi_Set.h"
#include <set>
#include<iterator>
#include<algorithm>
#include<vector>
#include<iostream>
#include<fstream>
#include<iomanip>

using namespace std;

#define N 25

Multi_Set::Multi_Set()
{
	try
	{
		int* mySet = new int[N]; // �������� �������� ���'��
	}
	catch (bad_alloc ba)
	{                                                            // �������� �� ��������� �������� ���'��� �� ���������
		cout << "Dynamic memory not allocated ! " << endl;
		cout << ba.what() << endl;	
	}
}

void Multi_Set::Enter_Set(int n)
{
	for (int i = 0; i < n; i++)
	{
		mySet.insert(rand() % 100);
	}
}

void Multi_Set::Enter_Set2(int n)
{
	for (int i = 0; i < n; i++)
	{
		int k;
		cout << "Multi_Set [ " << i << " ]= ";
		cin >> k;
		mySet.insert(k);
	}
}

void Multi_Set::Output_Set()
{
	copy(mySet.begin(), mySet.end(), ostream_iterator<int>(cout, "   "));
}

void Multi_Set::size()
{
	int k;
	k = mySet.size();
	cout << "Size of Multi Set is " << k << endl;
}

bool Multi_Set::empty()
{
	if (mySet.empty())
		return true;
	else return false;
}

void Multi_Set::swap(Multi_Set& set1)
{
	mySet.swap(set1.mySet);
}

void Multi_Set::include()
{
	int n1;
	cout << "Enter cout of elements that you want to add : ";
	cin >> n1;
	
	for (int i = 0; i < n1; i++)
	{
		int k;
		cout << "Enter element : ";
		cin >> k;
		mySet.insert(k);
	}
}

void Multi_Set::exlude()
{
	int n2;
	cout << "Enter the element that you want to delete : ";
	cin >> n2;
	mySet.erase(n2);
	cout << "Element " << n2 << " deleted ! " << endl;
}

void Multi_Set::find()
{
	int n3;
	cout << "Enter the element that you want to find : ";
	cin >> n3;

	if (*mySet.find(n3))
	{
		cout << "Element " << *mySet.find(n3) << " is a Multi Set;" << endl;
	}
	else
	{
		if (!*mySet.find(n3))
		{
			cout << "Multi Set do not have this element!" << endl;
		}
	}
}

void Multi_Set::accumulate()
{
	char t;
	int s = 0; 
	int p = 1;
	cout << "What you want to do ( + - suma elementiv; * - multiplied )" << endl;
	cin >> t;

	if (t == '+')
	{
		for (multiset<int>::iterator i = mySet.begin(); i != mySet.end(); ++i)
		{
			s += *i;
		}
		cout << "Suma elementiv Multi Set = " << s << endl;
	}
	else
	{
		if (t == '*')
		{
			for (multiset<int>::iterator i = mySet.begin(); i != mySet.end(); ++i)
			{
				p *= *i;
			}
		}
		cout << "Multiplied elementiv Multi Set = " << p << endl;
	}
}

void Multi_Set::for_each()
{
	int s, s1;
	cout << "Enter what element you want to change : ";
	cin >> s;
	cout << "Enter on what you want change : ";
	cin >> s1;

	if (*mySet.find(s))
	{
		mySet.erase(*mySet.find(s));
		mySet.insert(s1);
	}
	else cout << "Multi Set do not have that element " << endl;
}

Multi_Set::~Multi_Set()
{
	delete[]  &mySet;
}